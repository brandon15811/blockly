goog.provide('Blockly.PHP.pm');

goog.require('Blockly.PHP');

Blockly.BlockSvg.START_HAT = true;

Blockly.Blocks['pm_player_onJoin'] = {
    init: function () {
        this.jsonInit({
            "message0": "On player join",
            "args0": [],
            "message1": "%1",
            "args1": [
                {
                    "type": "field_paramlabel",
                    "name": "VAR",
                    "text": "player"
                }
            ],
            "message2": "do %1",
            "args2": [
                {
                    "type": "input_statement",
                    "name": "DO"
                }
            ],
            "colour": 230,
            "tooltip": "On player join event",
            "helpUrl": ""
        });
        Blockly.PHP.addReservedWords(['onJoin', 'event'].join(','));
    }
};

Blockly.Blocks['pm_event_getPlayer'] = {
    init: function () {
        this.jsonInit({
            "type": "pm_event_getPlayer",
            "message0": "Get player",
            "output": "Player",
            "colour": 230,
            "tooltip": "",
            "helpUrl": ""
        });
    },
};

Blockly.Blocks['pm_player_getName'] = {
    init: function () {
        this.jsonInit({
            "type": "pm_player_getName",
            "message0": "Get player name from %1",
            "args0": [
                {
                    "type": "input_value",
                    "name": "VALUE",
                    "check": "Player"
                }
            ],
            "output": "String",
            "colour": 230,
            "tooltip": "",
            "helpUrl": ""
        });
    },
};

Blockly.Blocks['pm_player_sendMessage'] = {
    /**
     * Block for print statement.
     * @this Blockly.Block
     */
    init: function () {
        this.jsonInit({
            "type": "pm_player_sendMessage",
            "message0": "Send message to player %1 message %2",
            "args0": [
                {
                    "type": "input_value",
                    "name": "PLAYER",
                    "check": "Player"
                },
                {
                    "type": "input_value",
                    "name": "MESSAGE",
                    "check": "String",
                    "align": "RIGHT"
                }
            ],
            "previousStatement": null,
            "nextStatement": null,
            "colour": 230,
            "tooltip": "",
            "helpUrl": ""
        });
    }
};

Blockly.Blocks['pm_command_onCommand'] = {
    init: function () {
        this.jsonInit({
            "message0": "On command /%1",
            "args0": [{
                "type": "input_value",
                "name": "COMMAND_NAME",
                "check": "String",
                "align": "RIGHT"
            }],
            "message1": "%1 %2",
            "args1": [
                {
                    "type": "field_paramlabel",
                    "name": "PLAYER",
                    "text": "player"
                },
                {
                    "type": "field_paramlabel",
                    "name": "ARGS",
                    "text": "args"
                }
            ],
            "message2": "do %1",
            "args2": [
                {
                    "type": "input_statement",
                    "name": "DO"
                }
            ],
            "colour": 230,
            "inputsInline": false,
            "tooltip": "On command event",
            "helpUrl": ""
        });
        //CommandSender $sender, Command $command, $label, array $args
        // Blockly.PHP.addReservedWords(['onCommand', 'sender', 'command', 'label', 'args', 'commands'].join(','));
    }
};

Blockly.Blocks['pm_config_put_key'] = {
    init: function () {
        this.jsonInit({
            "type": "pm_config_put_key",
            "message0": "Put config value %1 Key %2 Value %3",
            "args0": [
                {
                    "type": "input_dummy"
                },
                {
                    "type": "input_value",
                    "name": "KEY",
                    "align": "RIGHT"
                },
                {
                    "type": "input_value",
                    "name": "VALUE",
                    "align": "RIGHT"
                }
            ],
            "previousStatement": null,
            "nextStatement": null,
            "colour": 230,
            "tooltip": "",
            "helpUrl": "",
        });
    }
};

Blockly.Blocks['pm_config_get_key'] = {
    init: function () {
        this.jsonInit({
            "type": "pm_config_get_key",
            "message0": "Get config value %1 Key %2",
            "args0": [
                {
                    "type": "input_dummy"
                },
                {
                    "type": "input_value",
                    "name": "KEY",
                    "align": "RIGHT"
                }
            ],
            "colour": 230,
            "tooltip": "",
            "helpUrl": "",
            "output": null
        });
    }
};

Blockly.PHP['pm_player_onJoin'] = function (block) {
    console.log('hi');
    // For each loop.
    /*var variable0 = Blockly.PHP.variableDB_.getName(
        block.getFieldValue('VAR'), Blockly.Variables.NAME_TYPE);
    var argument0 = Blockly.PHP.valueToCode(block, 'LIST',
        Blockly.PHP.ORDER_ASSIGNMENT) || '[]';*/
    // Blockly.PHP.addReservedWords(['onJoin', 'event'].join(','));
    Blockly.PHP.addReservedWords('test');

    var branch = Blockly.PHP.statementToCode(block, 'DO');
    branch = Blockly.PHP.addLoopTrap(branch, block.id);
    console.log('hi');
    var code = '';
    code += 'public function onPlayerJoin(PlayerJoinEvent $event) {\n' +
        '$player = $event->getPlayer();\n' +
        branch +
        '}\n';
    // code += 'foreach (' + argument0 + ' as ' + variable0 +
    // ') {\n' + branch + '}\n';
    return code;
};

Blockly.PHP['pm_event_getPlayer'] = function (block) {
    var code = '';
    code += '$event->getPlayer()'
    return [code, Blockly.PHP.ORDER_NONE];
};

Blockly.PHP['pm_player_getName'] = function (block) {
    // Print statement.
    var player = Blockly.PHP.valueToCode(block, 'VALUE',
        Blockly.PHP.ORDER_ATOMIC);
    
    var code = player + '->getName()';
    return [code, Blockly.PHP.ORDER_NONE];
};

Blockly.PHP['pm_player_sendMessage'] = function (block) {
    // Print statement.
    var player = Blockly.PHP.valueToCode(block, 'PLAYER',
        Blockly.PHP.ORDER_ATOMIC);
    var message = Blockly.PHP.valueToCode(block, 'MESSAGE',
        Blockly.PHP.ORDER_ATOMIC);
    var code = player + '->sendMessage(' + message + ');\n';
    return code;
};

Blockly.PHP['pm_command_onCommand'] = function (block) {
    console.log('hi2');
    Blockly.PHP.addReservedWords('test');

    var branch = Blockly.PHP.statementToCode(block, 'DO');
    branch = Blockly.PHP.addLoopTrap(branch, block.id);
    console.log('hi');
    var commandName = Blockly.PHP.valueToCode(block, 'COMMAND_NAME',
        Blockly.PHP.ORDER_ATOMIC).slice(1, -1);
    Blockly.PHP.definitions_['commands'].push(commandName);
    var code = '';
    code += 'public function command_' + commandName + '(CommandSender $sender, Command $command, $label, array $args) {\n' +
        '$player = $this->getServer()->getPlayer($sender->getName());\n' +
        branch +
        '}\n';
    // code += 'foreach (' + argument0 + ' as ' + variable0 +
    // ') {\n' + branch + '}\n';
    return code;
};

Blockly.PHP['pm_config_put_key'] = function(block) {
    var value_key = Blockly.PHP.valueToCode(block, 'KEY', Blockly.PHP.ORDER_ATOMIC);
    var value_value = Blockly.PHP.valueToCode(block, 'VALUE', Blockly.PHP.ORDER_ATOMIC);
    var code = '$this->putConfigItem(' + value_key + ', ' + value_value + ');\n';
    return code;
};

Blockly.PHP['pm_config_get_key'] = function(block) {
    var value_key = Blockly.PHP.valueToCode(block, 'KEY', Blockly.PHP.ORDER_ATOMIC);
    var code = '$this->getConfigItem(' + value_key + ')';
    return [code, Blockly.PHP.ORDER_NONE];
};