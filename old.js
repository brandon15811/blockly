Blockly.Blocks['string_length'] = {
        init: function () {
            this.jsonInit({
                "type": "string_length",
                "message0": 'length of %1',
                "args0": [
                    {
                        "type": "input_value",
                        "name": "VALUE",
                        "check": "String"
                    }
                ],
                "output": "Number",
                "colour": 160,
                "tooltip": "Returns number of letters in the provided text.",
                "helpUrl": "http://www.w3schools.com/jsref/jsref_length_string.asp"
            });
        }
    };

    Blockly.JavaScript['string_length'] = function (block) {
        // String or array length.
        var argument0 = Blockly.JavaScript.valueToCode(block, 'VALUE',
            Blockly.JavaScript.ORDER_FUNCTION_CALL) || '\'\'';
        return [argument0 + '.length', Blockly.JavaScript.ORDER_MEMBER];
    };

// Blockly.PHP['pm_player_onJoin'] = function (block) {
//     // Is the string null or array empty?
//     var text = Blockly.PHP.valueToCode(block, 'VALUE',
//         Blockly.ORDER_ATOMIC) || '\'\'';
//     return ['empty(' + text + ')', Blockly.PHP.ORDER_FUNCTION_CALL];
// };

// var workspaceOptions = {
//     disabledPatternId: workspace.options.disabledPatternId,
//     parentWorkspace: workspace,
//     RTL: workspace.RTL,
//     oneBasedIndex: workspace.options.oneBasedIndex,
//     horizontalLayout: workspace.horizontalLayout,
//     toolboxPosition: workspace.options.toolboxPosition
// };
// var flyout = new Blockly.Flyout(workspaceOptions);
// flyout.autoClose = false;
// var a = flyout.createDom('svg')
// document.getElementsByClassName('injectionDiv')[0].append(a);
// flyout.init(workspace);
// flyout.setVisible(true);
// flyout.show(workspace.options.languageTree.childNodes);

/*Blockly.Blocks['jquery_element_text'] = {
 init: function () {
 this.jsonInit({
 "type": "jquery_element_text",
 "message0": 'text of element %1',
 "args0": [
 {
 "type": "input_value",
 "name": "VALUE",
 "check": "String"
 }
 ],
 "output": "String",
 "colour": 160,
 "tooltip": "Returns text of provided element.",
 "helpUrl": ""
 });
 }
 };

 Blockly.JavaScript['jquery_element_text'] = function (block) {
 // String or array length.
 var argument0 = Blockly.JavaScript.valueToCode(block, 'VALUE',
 Blockly.JavaScript.ORDER_FUNCTION_CALL) || '\'\'';
 return ['$(' + argument0 + ')' + '.text()', Blockly.JavaScript.ORDER_MEMBER];
 };*/

Blockly.Blocks['string_length'] = {
    init: function () {
        this.jsonInit({
            "type": "string_length",
            "message0": 'length of %1',
            "args0": [
                {
                    "type": "input_value",
                    "name": "VALUE",
                    "check": "String"
                }
            ],
            "output": "Number",
            "colour": 160,
            "tooltip": "Returns number of letters in the provided text.",
            "helpUrl": "http://www.w3schools.com/jsref/jsref_length_string.asp"
        });
    }
};

Blockly.PHP['string_length'] = function (block) {
    // String or array length.
    var argument0 = Blockly.PHP.valueToCode(block, 'VALUE',
            Blockly.PHP.ORDER_FUNCTION_CALL) || '\'\'';
    return [argument0 + '.length', Blockly.PHP.ORDER_MEMBER];
};