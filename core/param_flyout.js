/**
 * @license
 * Visual Blocks Editor
 *
 * Copyright 2011 Google Inc.
 * https://developers.google.com/blockly/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Flyout tray containing blocks which may be created.
 * @author fraser@google.com (Neil Fraser)
 */
'use strict';

goog.provide('Blockly.ParamFlyout');

goog.require('Blockly.Flyout');
goog.require('goog.events');

/**
 * Class for a flyout.
 * @param {!Object} workspaceOptions Dictionary of options for the workspace.
 * @constructor
 */
Blockly.ParamFlyout = function(workspaceOptions) {
    Blockly.ParamFlyout.superClass_.constructor.call(this, workspaceOptions);
};

goog.inherits(Blockly.ParamFlyout, Blockly.Flyout);

Blockly.Flyout.prototype.hide = function() {
    Blockly.ParamFlyout.superClass_.hide.call(this);
    Blockly.Events.fire(
        new Blockly.Events.Ui(Blockly.Flyout.startBlock_, 'click',
            undefined, undefined));
};