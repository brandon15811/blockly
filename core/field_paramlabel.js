/**
 * @license
 * Visual Blocks Editor
 *
 * Copyright 2015 Google Inc.
 * https://developers.google.com/blockly/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Date input field.
 * @author pkendall64@gmail.com (Paul Kendall)
 */
'use strict';

goog.provide('Blockly.FieldParamLabel');

goog.require('Blockly.Field');
goog.require('goog.date');
goog.require('goog.dom');
goog.require('goog.events');
goog.require('goog.i18n.DateTimeSymbols');
goog.require('goog.i18n.DateTimeSymbols_he');
goog.require('goog.style');
goog.require('goog.ui.DatePicker');
goog.require('Blockly.Bubble');


/**
 * Class for a parameter label field.
 * @param {string} name The initial content of the field.
 * @param {string=} opt_class Optional CSS class for the field's text.
 * @extends {Blockly.Field}
 * @constructor
 */
Blockly.FieldParamLabel = function(name, opt_class) {
  Blockly.FieldParamLabel.superClass_.constructor.call(this, name, opt_class);

  this.paramName = name;
  this.flyoutWorkspaceId = null;
  this.bubble_ = null;
  this.onParamFlyoutCloseWrapper_ = null;
  this.bubbleOpen = false;
};

goog.inherits(Blockly.FieldParamLabel, Blockly.Field);

Blockly.FieldParamLabel.prototype.updateEditable = function() {
  var group = this.fieldGroup_;
  Blockly.utils.addClass(group, 'blocklyNonEditableText');
  Blockly.utils.removeClass(group, 'blocklyEditableText');
};

Blockly.FieldParamLabel.prototype.showEditor_ = function() {
    if (this.bubbleOpen) {
        this.onParamFlyoutClose_(null, true);
        return;
    }
    var blockXY = this.sourceBlock_.getRelativeToSurfaceXY();
    var fieldXY = Blockly.utils.getRelativeXY(this.fieldGroup_);
    // var size = this.getSize();
    var newXY = new goog.math.Coordinate(
        blockXY.x + fieldXY.x,
        blockXY.y + fieldXY.y);
    var workspace = this.sourceBlock_.workspace;

    var workspaceOptions = {
        disabledPatternId: workspace.options.disabledPatternId,
        parentWorkspace: workspace,
        RTL: workspace.RTL,
        oneBasedIndex: workspace.options.oneBasedIndex,
        horizontalLayout: workspace.horizontalLayout,
        toolboxPosition: workspace.options.toolboxPosition
    };
    var flyout = new Blockly.Flyout(workspaceOptions);
    flyout.autoClose = true;
    var flyoutDom = flyout.createDom('svg');

    this.bubble_ = new Blockly.Bubble(
        /** @type {!Blockly.WorkspaceSvg} */ (workspace),
        flyoutDom, this.sourceBlock_.svgPath_, newXY, null, null);

    this.bubble_.setBubbleSize(150, 100);
    var bubbleSize = this.bubble_.getBubbleSize();

    this.bubble_.promote_();

    flyout.init(workspace);
    flyout.setVisible(true);
    //TODO: Use Blockly.Options.parseToolboxTree(document.getElementById()) for custom blocks
    var xmlString = `<xml>
          <block type="variables_get">
              <field name="VAR">${this.paramName}</field>
          </block>
      </xml>`;
    // var customBlocks = Blockly.Options.parseToolboxTree(document.getElementById('onCommand_blocks'));
    var customBlocks = Blockly.Options.parseToolboxTree(xmlString);
    // flyout.show(workspace.options.languageTree.childNodes);
    flyout.show(customBlocks.childNodes);

    flyout.width_ = bubbleSize.width;
    flyout.height_ = bubbleSize.height;
    flyout.svgGroup_.setAttribute("width", flyout.width_);
    flyout.svgGroup_.setAttribute("height", flyout.height_);
    // this.bubble_.setAnchorLocation(newXY);
    this.onParamFlyoutCloseWrapper_ = this.onParamFlyoutClose_.bind(this);
    workspace.addChangeListener(this.onParamFlyoutCloseWrapper_);
    this.bubbleOpen = true;
};

//TODO: Find a better event for this
Blockly.FieldParamLabel.prototype.onParamFlyoutClose_ = function(event, force) {
    if (force === true || event.type === Blockly.Events.CREATE) {
        console.log('closing');
        this.bubble_.dispose();
        console.log(this.bubble_);
        workspace.removeChangeListener(this.onParamFlyoutClose_);
        this.bubbleOpen = false;
    }
};

// /**
//  * Create the text for the warning's bubble.
//  * @param {string} text The text to display.
//  * @return {!SVGTextElement} The top-level node of the text.
//  * @private
//  */
// Blockly.FieldParamLabel.textToDom_ = function(text) {
//     var paragraph = /** @type {!SVGTextElement} */ (
//         Blockly.utils.createSvgElement('text',
//             {'class': 'blocklyText blocklyBubbleText',
//                 'y': Blockly.Bubble.BORDER_WIDTH},
//             null));
//     var lines = text.split('\n');
//     for (var i = 0; i < lines.length; i++) {
//         var tspanElement = Blockly.utils.createSvgElement('tspan',
//             {'dy': '1em', 'x': Blockly.Bubble.BORDER_WIDTH}, paragraph);
//         var textNode = document.createTextNode(lines[i]);
//         tspanElement.appendChild(textNode);
//     }
//     return paragraph;
// };